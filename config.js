module.exports = {
  siteTitle: 'PSC Challenger', // Site title.
  siteTitleAlt: 'PSC Challenger Blog', // Alternative site title for SEO.
  siteLogo: '/icons/icon-512x512.png', // Logo used for SEO and manifest.
  siteUrl: 'https://blog.keralapsc.app', // Domain of your website without pathPrefix.
  websiteUrl: 'https://blog.keralapsc.app',
  pathPrefix: '', // Prefixes all links. For cases when deployed to example.github.io/gatsby-starter-business/.
  siteDescription: 'PSC challenger is the #1 Kerala PSC preparation mobile application which provides latest updates, news, exam syllabus, previous questionpaper', // Website description used for RSS feeds/meta description tag.
  siteRss: '/rss.xml',
  googleTagManagerID: process.env.GTM_ID || '', // GTM tracking ID.
  disqusShortname: 'https-theleakycauldronblog-com', // Disqus shortname.
  userName: 'Vivek',
  userTwitter: 'pscchallenger',
  userLocation: 'Kerala, India',
  copyright: 'Copyright © PSC Challenger. ', // Copyright string for the footer of the website and RSS feed.
  themeColor: '#676767', // Used for setting manifest and progress theme colors.
  backgroundColor: '#ffffff', // Used for setting manifest background color.
}
