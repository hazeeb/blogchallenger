import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import _ from 'lodash'
import Img from 'gatsby-image'
import './styles.scss'
import 'prismjs/themes/prism-tomorrow.css'
import Content from '../Content'

const ArticleTemplate = ({
  content,
  date,
  contentComponent,
  cover,
  tags,
  title,
  author,
}) => {
  const PostContent = contentComponent || Content

  return (
    <article className='athelas pb3'>
      <section className='center'>
        <header className='avenir'>
          <h3 className='f2 f2-m measure lh-title'>{title}</h3>
          <div className='flex db'>
            <time style={{color:'grey'}} className='f5 f4-l db fw1 baskerville mb4-l mb2'>{author} | {date}</time>
          </div>
        </header>
      </section>
      {/* {!!cover && !!cover.childImageSharp
        ? <Img
          className='w-100 dib f3'
          fluid={cover.childImageSharp.fluid}
          alt={title}
        />
        : <img
          className='w-100 dib f3'
          src={cover.publicURL}
          alt={title}
        />} */}
      <section className='mw8 center'>
        <div>
          <PostContent content={content} className='db center f5 f4-ns lh-copy' />
        </div>
      </section>
      <div className='inline-flex flex-wrap'>
              {tags && tags.length &&
              tags.map(tag => (
                <Link
                  to={`/tags/${_.kebabCase(tag)}`}
                  key={tag}
                  className='no-underline black dim avenir'
                >
                  <small className='f6 f4-l fw1'>#{tag}&nbsp;&nbsp;</small>
                </Link>
              ))}
            </div>
    </article>
  )
}

ArticleTemplate.propTypes = {
  content: PropTypes.string,
  date: PropTypes.string,
  contentComponent: PropTypes.func,
  cover: PropTypes.object,
  tags: PropTypes.array,
  title: PropTypes.string,
  author: PropTypes.string,
}

export default ArticleTemplate
