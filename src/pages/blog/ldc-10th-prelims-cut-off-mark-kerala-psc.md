---
templateKey: article-page
title: LDC 10th Prelims cut off mark | Kerala PSC
author: Anand
date: 2021-09-18T11:46:04.317Z
cover: /img/prlims.png
meta_title: Kerala psc LDC 10th prelims result published
meta_description: |-
  കേരള പി.എസ്.സി നടത്തിയ 10th ലെവൽ പ്രിലിമിനറി പരീക്ഷയുടെ ഫലം പ്രഖ്യാപിച്ചു.
  cut off,  psc preliminary short list
tags:
  - preliminary exam result
---

ഷോർട്ട് ലിസ്റ്റ് download ചെയാൻ താഴെ നൽകിയ ജില്ല ക്ലിക്ക് ചെയ്യുക. 

[കാസർകോഡ്](https://drive.google.com/file/d/1ozbYdyiLoO7N_aaB-wNu8pLE5FqrA1LP/view?usp=sharing)

[കണ്ണൂർ](https://drive.google.com/file/d/1Yx9vDUcn6ueUfgIEIukjakwsuquKeyQj/view?usp=sharing)

[വയനാട്](https://drive.google.com/file/d/16SETfYJ1feOe5oZI1DiiLhS7rM58PsZb/view?usp=sharing)

[കോഴിക്കോട്](https://drive.google.com/file/d/1yGYsy8P1hPA6dj1lCzajzmBVAPDu1dj5/view?usp=sharing)

[മലപ്പുറം](https://drive.google.com/file/d/13zl_gkFVDfH6aBSSletfWjpErdEVqruT/view?usp=sharing)

[പാലക്കാട്](https://drive.google.com/file/d/17FXvJRF2Obsh8zIoFQqLoqxEmS7zVWaF/view?usp=sharing)

[ത്യശൂർ](https://drive.google.com/file/d/1T3JlBOjtazMsnI0ccLdExU22LpEFet1C/view?usp=sharing)

[എറണാകുളം](https://drive.google.com/file/d/1oapI4sS73T1I9T8kFgCfVC5OzMRHPH5o/view?usp=sharing)


[ഇടുക്കി](https://drive.google.com/file/d/1ZXGcZlBEUqjafP6CyeI30-o8CGmb9w39/view?usp=sharing)


[കോട്ടയം](https://drive.google.com/file/d/1QTAGQ_CXcG-YQFWWSwOmkBDxLtKyKbWH/view?usp=sharing)

[ആലപ്പുഴ](https://drive.google.com/file/d/1SzKGVw7vy4jMDuTnFlW0ik4hdXO4J0AP/view?usp=sharing)


[പത്തനംതിട്ട](https://drive.google.com/file/d/1KSQ7KkMX7y9snGxSPpp0MEUH4JAjH33l/view?usp=sharing)


[കൊല്ലം](https://drive.google.com/file/d/1qveUhtgte84fCGwJReiJ84LH2Cz2JCd9/view?usp=sharing)


[തിരുവനന്തപുരം](https://drive.google.com/file/d/1Tl_yPA9arNGJAV0H_VlX-aGQLmFhnSuu/view?usp=sharing)

![alt text for screen readers](https://i.imgur.com/8oEkzcU.png "Text to show on mouseover")
