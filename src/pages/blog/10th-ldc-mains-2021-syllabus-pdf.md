---
templateKey: article-page
title: 10th LDC Mains 2021 syllabus pdf
author: Anand
date: 2021-10-22T19:20:35.171Z
cover: /img/LGSsyllabus2021.png
meta_title: LDC Mains PDF
meta_description: Download LDC Mains pdf file
tags:
  - syllabus
---

<a href="https://www.keralapsc.gov.in/sites/default/files/2021-06/ldc_main_exam_final_malayalam___english.pdf">Download PDF</a>

<iframe src="https://drive.google.com/file/d/1kJJeabYl-NH4nsNV8kLs-qTUYESbUfMx/preview" width="100%" height="100%">
</iframe>

