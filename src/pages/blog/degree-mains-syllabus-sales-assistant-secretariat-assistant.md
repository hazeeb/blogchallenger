---
templateKey: article-page
title: 'Degree Mains Syllabus - Sales Assistant, Secretariat Assistant'
author: Admin
date: 2022-01-11T12:19:21.861Z
cover: /img/secretariat-assistant.png
meta_title: 'Degree mains syllabus - Sales Assistant, Special Branch Assistant, '
meta_description: 'Assistant in Finance Department, Assistant/Auditor in Govt. Secretariat'
tags:
  - syllabus
  - degree-mains
---
2022 ഡിഗ്രി mains പരീക്ഷയുടെ സിലബസ് kerala psc പ്രസിദ്ധീകരിച്ചു.
മാർക്ക് distribution, special topic എന്നിവ അടങ്ങിയിട്ടുള്ള syllabus ആണ് psc പ്രസിദ്ധീകരിച്ചത്.

താഴെ നൽകിയ തസ്തിക അനുസരിച്ച് നിങ്ങൾക്ക് സിലബസ് download ചെയ്യാം.

> - Secretariat Assistant
> - Sales Assistant
> - Special Branch Assistant
>
>  [Download PDF Syllabus](https://drive.google.com/file/d/1d0UPzc_0SzWaHy1w3QT6m4Mk_bB9sdJy/preview)




> - Senior Super Superintendent
> - Assistant Treasury Officer
> - Sub Treasury Officer
>
>  [Download PDF Syllabus](https://www.keralapsc.gov.in/sites/default/files/2022-01/II.Sr_.%20Supdt.%20&%20SO%20KPSC.pdf)


> - Armed Police Sub Inspector
> - Sub Inspector Of Police
> - Women Sub Inspector
>
>  [Download PDF Syllabus](https://www.keralapsc.gov.in/sites/default/files/2022-01/III.S%20I.pdf)

> - Assistant Jailor
> - Subjail/supervisor-open Prison/supervisor-borstal School/armoroursica Etc
>
>  [Download PDF Syllabus](https://www.keralapsc.gov.in/sites/default/files/2022-01/IV.%20Assistant%20Jailor.pdf)

> - Excise Inspector
>
>  [Download PDF Syllabus](https://www.keralapsc.gov.in/sites/default/files/2022-01/V.%20%20Excise%20Inspector.pdf)

> - Assistant (Kerala Administrative Tribunal)
>
>  [Download PDF Syllabus](https://www.keralapsc.gov.in/sites/default/files/2022-01/VI.%20%20Asst%20-%20KAT.pdf)

> - Divisional Accountant
>
>  [Download PDF Syllabus](https://www.keralapsc.gov.in/sites/default/files/2022-01/IX.%20%20Divisional%20Accountant%20%20.pdf)

> - Confidential Assistant Grade II
> - Typist Clerk Gr II
>
>  [Download PDF Syllabus](https://www.keralapsc.gov.in/sites/default/files/2022-01/VIII.%20%20Typist%20Clerk.pdf)


> - Data Entry Operator 
>
>  [Download PDF Syllabus](https://www.keralapsc.gov.in/sites/default/files/2022-01/VII.Data%20Entry%20Operator.pdf)

> - Assistant Director Of National Savings
>
>  [Download PDF Syllabus](https://www.keralapsc.gov.in/sites/default/files/2022-01/X.%20Assistant%20National%20Savings.pdf)

> - Junior Receptionist
>
>  [Download PDF Syllabus](https://www.keralapsc.gov.in/sites/default/files/2022-01/XI.%20Receptionist.pdf)

> -Junior Manager (General) 
>
>  [Download PDF Syllabus](https://www.keralapsc.gov.in/sites/default/files/2022-01/XII.%20%20Jr.%20Manager.pdf)





