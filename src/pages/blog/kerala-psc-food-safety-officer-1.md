---
templateKey: article-page
title: Kerala PSC Food Safety Officer
author: Vaibhav
date: 2020-03-05T09:58:44.257Z
cover: /img/foodsafety.png
meta_title: Kerala PSC Food Safety Officer
meta_description: >-
  Kerala PSC Food Safety Officer Salary, last date to apply and previous year
  question paper.
tags:
  - kerala psc
  - foodsafety
---
ഭക്ഷ്യ സുരക്ഷാ ഓഫിസർ തസ്തികയിലേക്കുള്ള psc പരീക്ഷ 2020 മെയ് മാസം നടക്കുന്നതാണ്.\
ഇംഗ്ലീഷിൽ നടത്തപ്പെടുന്ന ഈ പരീക്ഷയിൽ 100 മാർക്കിന്റെ 100 ചോദ്യങ്ങളാണ് ഉണ്ടാവുക, അതിൽ 70 ചോദ്യങ്ങളും ഭക്ഷ്യ സുരക്ഷയുമായി ബന്ധപ്പെട്ടവിഷയങ്ങളിൽ നിന്നും, ബാക്കി 30 മാർക്കിന് പൊതുവിവരം, mental ability, reasoning, തുടങ്ങിയ വിഷയങ്ങളിൽ നിന്നുമായിരിക്കും. വിശദമായ സിലബസ് ലഭിക്കാൻ ലിങ്ക് ക്ലിക്ക് ചെയ്യുക. താഴെ കൊടുത്തിരിക്കുന്നു. \
\
[Download Syllabus](https://www.keralapsc.gov.in/sites/default/files/2020-02/FSO.pdf)\
\
Category Number : 499/2019\
Salary of Food Safety Officer : Rs.27800 – 59400/\
Last date to apply : 19 Feb, 2020

പ്രായം: **18 - 26** \
(ഉദ്യോഗാർത്ഥികൾ 2/jan/1983 - 1/jan/2001 -നും ഇടയിൽ ജനിച്ചവരായിരിക്കണം)

യോഗ്യത:

1. കേരളത്തിലെ ഏതെങ്കിലും സർവകലാശാലയിൽ നിന്നും താഴെകൊടുത്ത കോഴ്സുകളിൽ ബിരുദം.

   * ഫുഡ് ടെക്നോളജി
   * ഡയറി ടെക്നോളജി
   * വെറ്റിനറി സയൻസ്
   * ബയോ കെമിസ്ട്രി
   * മൈക്രോ ബയോളജി
   * ഓയിൽ ടെക്നോളജി

അല്ലെങ്കിൽ കെമിസ്ട്രിയിൽ ബിരുദാനന്തര ബിരുദം

കൂടുതൽ അറിയാൻ[ ഈ ലിങ്ക്](https://www.keralapsc.gov.in/sites/default/files/2020-01/499-2019.pdf) സന്ദർശിക്കുക.
