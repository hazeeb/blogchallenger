---
templateKey: article-page
title: 10th Preliminary 2022 Syllabus - Kerala PSC
author: Vaibhav
date: 2022-03-15T12:59:27.894Z
cover: /img/10thprelims-min (1).png
meta_title: 10th preliminary syllabus
meta_description: >-
  kerala psc 10th preliminary syllabus 2022 exam, ldc, vfa syllabus, village
  field assistant syllabus.
tags:
  - syllabus
---
പത്താം തരം പ്രാഥമിക പരീക്ഷയുടെ സിലബസ് താഴെ നൽകുന്നു

<a href="https://drive.google.com/file/d/1HeMdllj4Pv98O9Tn01IBAgAUpFTs6XwW/preview"> Download PDF </a>

10th preliminary പരീക്ഷയിൽ മാർക്ക് വിഹിതം psc നൽകിയിട്ടുണ്ട്.
