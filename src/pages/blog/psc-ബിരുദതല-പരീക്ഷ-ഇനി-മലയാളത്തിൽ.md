---
templateKey: article-page
title: PSC ബിരുദതല പരീക്ഷ ഇനി മലയാളത്തിൽ!
author: Arjun
date: 2020-05-14T04:17:57.491Z
cover: /img/degreeexam.png
meta_title: PSC degree exam in malayalam medium
meta_description: PSC degree exam in malayalam medium
tags:
  - psc
---
പത്താം ക്ലാസ് പരീക്ഷകൾ പോലെ ബിരുദതല പരീക്ഷകൾക്കും ചോദ്യങ്ങൾ മലയാളത്തിലാക്കാൻ psc കമ്മീഷൻ തീരുമാനിച്ചു.

കന്നഡ, തമിഴ്‌ മാധ്യമങ്ങളിലും ചോദ്യങ്ങൾ ലഭ്യമാകും.
