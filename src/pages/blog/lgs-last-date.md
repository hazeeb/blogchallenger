---
templateKey: article-page
title: LGS company board last date to apply - Kerala PSC
author: Admin
date: 2022-01-19T05:52:13.202Z
cover: /img/lgs-lastate.png
meta_title: 'Last date of LGS company board, prison officer'
meta_description: >-
  Last date to apply for LGS company board, assistant prison officer, typist
  grade II is January 19.
tags:
  - lgs
---
താഴെ നൽകിയ പോസ്റ്റുകൾക്ക് ഇന്നാണ് (ജനുവരി 19) അപേക്ഷ സമർപ്പിക്കേണ്ട അവസാന തീയതി.ഇനിയും അപേക്ഷിച്ചില്ലെങ്കിൽ ഇപ്പോൾ തന്നെ അപേക്ഷിക്കുക.
കാറ്റഗറി നമ്പറുകളും മറ്റ് പ്രധാനപ്പെട്ട വിവരങ്ങളും നൽകിയിട്ടുണ്ട്.
എങ്ങനെയാണു അപേക്ഷിക്കേണ്ട രീതിയും താഴെ നൽകിയിട്ടുണ്ട്.
 
## പോസ്റ്റുകൾ
-   Last Grade Servants (**LGS**) - 609/2021

-  Assistant Prison Officer (600/2021)
    
    -   ശമ്പളം - ₹ 20,000-45,800/-(PR)
    -   ഒഴിവ് - 30
-  Junior Assistant (600/2021)
    
    -   ശമ്പളം - ₹5650-8790
    -   ഒഴിവ് - 6
-   Typist Gr.II - 608/2021
    

    
-   Accountant/accounts Clerk/assistant Manager/ Assistant Gr. II - 610/2021
    
-   Store Assistant Gr.II - 611/2021
    





## എങ്ങനെയാണ് പരീക്ഷക്ക് അപേക്ഷിക്കേണ്ടത് ?

1.  ഉദ്യോഗാർത്ഥികൾ കേരള പബ്ലിക് സർവീസ് കമ്മീഷന്റെ ഔദ്യോഗിക വെബ്‌സൈറ്റായ [www.keralapsc.gov.in](http://www.keralapsc.gov.in) ഒറ്റത്തവണ രജിസ്‌ട്രേഷൻ സമ്പ്രദായം അനുസരിച്ച് ആദ്യം രജിസ്റ്റർ ചെയ്യണം.
2.  രജിസ്റ്റർ ചെയ്തിട്ടുള്ള ഉദ്യോഗാർത്ഥികൾ അവരുടെ User ID, Password ഉപയോഗിച്ച് പ്രൊഫൈലിൽ Login ചെയ്യുക.
3. താഴെ നൽകിയ പോലെ “notification എന്ന ബട്ടൺ ക്ലിക്ക് ചെയ്യുക.
![Image](https://challenger-images.sgp1.digitaloceanspaces.com/website/notifiaction.png)
4.  താഴെ നോട്ടിഫിക്ഷൻ നമ്പർ ടൈപ്പ് ചെയ്യുക.
![image](https://challenger-images.sgp1.digitaloceanspaces.com/website/category.png)
5.  പരീക്ഷക്ക് "Apply" ബട്ടൺ ക്ലിക്ക് ചെയ്യുക.
![image](https://challenger-images.sgp1.digitaloceanspaces.com/website/apply.png)
6.  ഓൺലൈൻ അപേക്ഷയുടെ പ്രിന്റൗട്ടോ സോഫ്റ്റ് കോപ്പിയോ സൂക്ഷിക്കുക.
