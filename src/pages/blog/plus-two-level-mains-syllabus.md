---
templateKey: article-page
title: Plus two level mains syllabus
author: Anand
date: 2021-10-04T13:49:01.436Z
cover: /img/plustwo-syllabus.png
meta_title: 'Download Plus two level mains syllabus of kerala psc '
meta_description: >-
  Download Civil police officer, fireman, firewoman, inspecting assistant (legal
  metrology), civil excise officer, beat forest officer, computer assistant,
  stenographer, office assistant, office superintendent syllabus now.
tags:
  - Syllabus
---
Download Plus two mains syllabus

[Civil Police Officer](https://www.keralapsc.gov.in/sites/default/files/2021-08/Civil%20Police%20Officer%20-%20Police_0.pdf)

[Fireman/Fire woman](https://www.keralapsc.gov.in/sites/default/files/2021-08/Fireman%20Fire%20%26%20Rescue.pdf)

[Inspecting Assistant](https://www.keralapsc.gov.in/sites/default/files/2021-08/Inspecting%20Asst%20-%20Legal%20Metrology_0.pdf) (Legal Metrology)

[Civil Excise Officer](https://www.keralapsc.gov.in/sites/default/files/2021-08/Civil%20Excise%20Officer%20.pdf)

[Beat Forest Officer](https://www.keralapsc.gov.in/sites/default/files/2021-08/Beat%20Forest%20Officer_0.pdf)


[Computer Assistant Gr.II/Stenographer/Office Assistant](https://www.keralapsc.gov.in/sites/default/files/2021-08/COMPUTER%20ASSISTANT%20%20-%20Various_0.pdf)

[Office Superintendent](https://www.keralapsc.gov.in/sites/default/files/2021-08/Office%20Suprt.%20-%20%20KPSC_0.pdf)
