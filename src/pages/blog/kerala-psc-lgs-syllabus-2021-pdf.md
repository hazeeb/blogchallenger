---
templateKey: article-page
title: Kerala PSC LGS syllabus 2021 Pdf
author: Vaibhav
date: 2021-06-27T11:53:05.529Z
cover: /img/LGSsyllabus2021.png
meta_title: Kerala PSC LGS syllabus 2021 Pdf
meta_description: Download kerala psc new last grade servant exam syllabus 2021 pdf
tags:
  - LGS
---
Latest kerala psc last grade servants examination syllabus 2021. </br>Click on the following link to download the pdf file of **LGS syllabus 2021**.

<a href="https://www.keralapsc.gov.in/sites/default/files/2021-06/syll.pdf"  target="_blank"> Download Syllabus</a>
