---
templateKey: article-page
title: ഏപ്രിൽ 30 വരെയുള്ള PSC പരീക്ഷകൾ മാറ്റിവെച്ചു
author: Hisham
date: 2020-03-23T14:34:37.092Z
cover: /img/exampostpo.png
meta_title: kerala psc exam postponed
meta_description: ഏപ്രിൽ 30 വരെയുള്ള PSC പരീക്ഷ മാറ്റിവെച്ചു
tags:
  - keralapsc
---
കൊറോണ വൈറസിന്റെ വ്യാപനത്തിന്റെ പശ്ചാത്തലത്തിൽ 2020 ഏപ്രിൽ 20 വരെ പി.എസ്.സി നടത്താനിരുന്ന എല്ലാ പരീക്ഷകളും മാറ്റിവെച്ചിരിക്കുന്നു എന്ന് കൺട്രോളർ ഓഫ് എക്‌സാമിനേഷൻസ് അറിയിച്ചു.

![alt text](https://scontent.fcok1-1.fna.fbcdn.net/v/t1.0-9/90915282_529362491085631_5085710190108475392_n.png?_nc_cat=1&_nc_sid=8024bb&_nc_ohc=8HAn_FnlRrIAX9Cwyyw&_nc_ht=scontent.fcok1-1.fna&oh=97f2a6c8488989dfe2bb5e88d79ed341&oe=5EA003CF "PSC exam postponed")

പുതുക്കിയ തീയതി പിന്നീട് പ്രസിദ്ധീകരിക്കുമെന്ന് അറിയിച്ചു.

ഏപ്രിൽ 14 വരെ നടത്താനിരുന്ന എല്ലാ പരീക്ഷകളും മാറ്റിവെച്ചതായി മുൻപ് പി.എസ്.സി അറിയിച്ചിരുന്നു.

![alt text](https://scontent.fcok1-1.fna.fbcdn.net/v/t1.0-9/90489340_525769468111600_2187592436316897280_n.png?_nc_cat=108&_nc_sid=8024bb&_nc_ohc=5lMa3LjR55cAX8H2eIw&_nc_ht=scontent.fcok1-1.fna&oh=26ef88a5ba072f797537251e417636cb&oe=5E9DE514 "PSC exam postponed")
