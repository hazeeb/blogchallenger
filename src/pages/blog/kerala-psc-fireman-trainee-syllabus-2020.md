---
templateKey: article-page
title: Kerala PSC Fireman (trainee) syllabus 2020
author: Haseeb
date: 2020-03-18T08:13:41.042Z
cover: /img/fireman.png
meta_title: Kerala PSC Fireman (trainee) syllabus 2020
meta_description: >-
  Kerala PSC Fireman (trainee) syllabus 2020, പരീക്ഷയ്ക്ക് എന്തെല്ലാം പഠിക്കണം
  എങ്ങനെ പഠിക്കണം ,ഉയർന്ന score എങ്ങനെ നേടാം എന്ന് അറിയാം.
tags:
  - kerala psc
  - firemans
---
പി.എസ്.സി ഓദ്യോഗികമായി വെബ്സൈറ്റ് വഴി അറിയിച്ച ഫയർമാൻ പരീക്ഷയ്ക്കുള്ള  സിലബസാണ്‌ താഴെ കൊടുത്തിരിക്കുന്നത്.

- Biology
- Geography
- First Aid
- Economics
- Physics and chemistry
- Indian Constitution
- Computer Science
- Arts, Sports, Science
- Current Affairs
- General English
- Simple Arithmetic, Mental Ability

പി.എസ്.സി അടുത്തിടെ ഉൾപ്പെടുത്തിയ ഒരു വിഷയമാണ് പ്രഥമ ശുശ്രൂഷ. ഈ വിഷയത്തിൽ ഏതെല്ലാം പഠിക്കണം എന്ന് ഉദ്യോഗാർത്ഥികൾക്ക് സംശയമുണ്ട്. എന്നാൽ  പി.എസ്.സി ഈ വർഷം നടത്തിയ ഫയർമാൻ ഡ്രൈവർ പരീക്ഷയിൽ പ്രഥമ ശുശ്രൂഷ എന്ന വിഷയം ഉൾപ്പെടുത്തിയിരുന്നു.
അത് പ്രകാരം CPR, പാമ്പ് കടിയേൽക്കുക, പ്രഥമ ശുശ്രൂഷ facts, Nosebleed, എല്ല് പൊട്ടുന്നതിന്റെ പ്രഥമ ശുശ്രൂഷ എന്നിങ്ങനെ വിവിധ പ്രഥമ ശുശ്രൂഷ വിഷയങ്ങളിൽ പരീക്ഷയ്ക്ക് വരാവുന്ന ചോദ്യങ്ങൾ വിഷയങ്ങൾ തരം തിരിച്ച് psc challenger അപ്പ്ലിക്കേഷനിൽ ലഭ്യമാണ്.

അത് പോലെ ഫയർമാൻ പരീക്ഷയ്ക്ക് തയ്യാറെടുക്കുന്നവർ നിർബന്ധമായും പരിശീലിക്കേണ്ട ചോദ്യപേപ്പർ ഗെയിം പോലെ നിങ്ങൾക്ക്  psc challenger അപ്പ്ലിക്കേഷനിൽ പരിശീലിക്കാവുന്നതാണ്.


| Fireman             |  First Aid |
:-------------------------:|:-------------------------:
![](https://i.imgur.com/SLGvexZ.jpg)  |  ![](https://i.imgur.com/PIqe6Fr.jpg)  |

ആപ്പ്ലിക്കേഷനിൽ മുൻ വർഷങ്ങളിൽ നടന്ന ഫയർമാൻ പരീക്ഷയുടെ ചോദ്യപേപ്പർ ഉൾപ്പെടുത്തിയിട്ടുണ്ട്.

✅ <a href="https://play.google.com/store/apps/details?id=com.pscchallengerapp&referrer=utm_source%3Dblog%26utm_medium%3Dblog" target="_BLANK">Click here </a>  to download PSc Challenger mobile application from playstore.
