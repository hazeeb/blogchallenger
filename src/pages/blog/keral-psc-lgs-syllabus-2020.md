---
templateKey: article-page
title: Keral PSC LGS Syllabus 2020
author: Haseeb
date: 2020-08-16T10:24:03.627Z
cover: /img/lgssyllabus.png
meta_title: LGS Exam Syllabus 2020
meta_description: Kerala PSC LGS Exam syllabus 2020
tags:
  - LGS
---
## ലഘുഗണിതം

1. സംഖ്യകളും അടിസ്ഥാന ക്രിയകളും
2. ഭിന്നസംഖ്യകളും ദശാംശ സംഖ്യകളും
3. ശതമാനം 
4. ലാഭവും നഷ്ടവും 
5. സാധാരണ പലിശയും കൂട്ടുപലിശയും 
6.  അംശബന്ധവും അനുപാതവും 
7.  സമയവും ദൂരവും 
8. സമയവും പ്രവൃത്തിയും 
9. ശരാശരി 
10. ക്യത്യങ്കങ്ങൾ 
11. ജ്യാമിതീയ രൂപങ്ങളുടെ ചുറ്റളവ്, വിസ്തീർണ്ണം, വ്യാപ്തം 
12. പ്രാഗ്രഷനുകൾ 

## മാനസികശേഷി

1. സീരീസ് 
2. ഗണിതചിഹ്നങ്ങൾ ഉപയോഗിച്ചുള്ള പ്രശനങ്ങൾ 
3. സ്ഥാനനിർണ്ണായ പരിശോധനന 
4. സമാനബന്ധങ്ങൾ 
5. ഒറ്റയാനെ കണ്ടെത്തുക 
6. സംഖ്യാവലോകന പ്രശ്ങ്ങൾ 
7. കോഡിങ് ഡികോഡിങ് 
8. കുടുംബബന്ധങ്ങൾ 
9.  ദിശാബോധം 
10. ക്ലോക്കിലെ സമയവും കോണളവും 
11.  ക്ളോക്കിലെ സമയവും പ്രതിബിംബവും 
12. കലണ്ടറും തിയതിയും 
