---
templateKey: article-page
title: Kerala psc new degree prelims syllabus 2021
author: Vaibhav
date: 2021-09-27T05:58:02.466Z
cover: /img/Frame 1.png
meta_title: Degree prelims exam syllabus pdf
meta_description: Download pdf file of Kerala PSC degree preliminary exam syllabus.
tags:
  - syllabus
---
You can download degree preliminary exam detailed syllabus (pdf file) published Kerala PSC official website by clicking following link below.

[Download](https://keralapsc.gov.in/sites/default/files/inline-files/DEGREE%20SY.pdf)

