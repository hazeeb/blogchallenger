---
templateKey: article-page
title: Assistant salesman exam syllabus 2020
author: Vaibhav
date: 2020-10-02T18:29:59.870Z
cover: /img/assistantsalesman.png
meta_title: Assistant salesman exam syllabus 2020
meta_description: >-
  Kerala Psc Assistant salesman (civil supplies) exam syllabus 2020, exam
  pattern, previous year question paper and mock exams.
tags:
  - psc
---

അസിസ്റ്റന്റ് സെൽസ്‌മാൻ കേരള സ്റ്റേറ്റ് സിവിൽ സപ്ലൈസ് കോർപറേഷൻ ലിമിറ്റഡ് പരീക്ഷയുടെ വിജ്ഞാപനം വന്നു.
ഉദ്യോഗാർത്ഥികൾ അപേക്ഷിക്കേണ്ട അവസാന തീയതി - ഒക്ടോബർ 21, 2020

2 ഘട്ടമായാണ് പരീക്ഷ നടത്തുന്നത്. 
പത്താം തലത്തിലുള്ള പ്രാഥമിക പരീക്ഷ വിജയിക്കുന്നവർക്ക് മുഖ്യ പരീക്ഷ കൂടെയുണ്ടാകും.

2016-ൽ നാല് ലക്ഷം അപേക്ഷകർ ഉണ്ടായിരുന്ന ഈ പരീക്ഷയ്ക്ക് ഇത്തവണ ആറ് ലക്ഷം അപേക്ഷകരെ പ്രതീക്ഷിക്കുന്നു. കഴിഞ്ഞ തവണ രണ്ടായിരത്തോളം നിയമനങ്ങൾ നടന്നിരുന്നു.

യോഗ്യത

1.  SSLC പാസായിരിക്കണം (അല്ലെങ്കിൽ തത്തുല്യമായ യോഗ്യത)
2.  18-36 വയസ്സിന് ഇടയിലായിരിക്കണം
3.  പിന്നോക്ക വിഭാഗക്കാർക്ക് പ്രായപരിധിയിൽ ഇളവുകളുണ്ടാകും.

2016-ലെ പരീക്ഷരീതിയും മാർക്ക് വിതരണവും:
1. പൊതു വിജ്ഞാനം, ആനുകാലികം, അടിസ്ഥാന ശാസ്ത്രം, കേരളം അടിസ്ഥാന വസ്തുതകൾ, കേരള നവോത്ഥാനം
2.  ഇംഗ്ലീഷ്
3. ലഘു ഗണിതം
4. മാനസിക ശേഷിയും, നിരീക്ഷണപാടവ പരിശോധന 
	
2020-ലെ സിലബസ് ഇത് വരെ പി.എസ്.സി പുറത്ത് വിട്ടിട്ടില്ല.

Click here to download official kerala psc assistant salesman civil supplies examination notification.
