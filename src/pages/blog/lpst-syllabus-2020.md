---
templateKey: article-page
title: LPST Syllabus 2020
author: Hisham
date: 2020-04-25T06:12:37.162Z
cover: /img/lpst.png
meta_title: LPST Syllabus 2020
meta_description: LPST Syllabus 2020
tags:
  - PSC
---
LPST Syllabus 2020

1. GK & Current Affairs (40 Marks)
2. Science (20 Marks)
3. Maths & Mental Ability (20 Marks)
4. Psychology & Pedagogy (20 Marks)
