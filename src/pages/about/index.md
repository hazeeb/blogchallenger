---
templateKey: about-page
title: About This Blog
meta_title: About | Psc challenger
meta_description: >-
  PSC Challenger is No:1 learning platform for kerala psc examination.
---
**PSC Challenger** is the #1 learning platform for **kerala psc examinations.**.
This is a real-time quiz game that gives you an adrenaline rush.

You can download this application from [Google Playstore](https://play.google.com/store/apps/details?id=com.pscchallengerapp), or download it from [here](https://play.google.com/store/apps/details?id=com.pscchallengerapp).


**Using the app is simple**:
- Register with your mobile number and choose the correct Kerala Districts PSC.
- Click Play Now to get questions
- You have 15 seconds to answer each question. Select the correct answer as fast as possible to get the best score
- After the game is over, you'll see the correct answers to each question
- You spend 10 coins for each game and the winner gets 20 coins